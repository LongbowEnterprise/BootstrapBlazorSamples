﻿using Microsoft.AspNetCore.Components;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Aufo.Web
{
    public sealed partial class Foo : ComponentBase, IDisposable
    {
        [Parameter]
        public string Url { get; set; }

        [Inject]
        private IDbContextFactory<BindItemDbContext> DbFactory { get; set; }

        private BindItemDbContext Context { get; set; }

        private IQueryable<BindItem> Items { get; set; }

        private static Random rnd = new Random();

        protected override void OnInitialized()
        {
            base.OnInitialized();

            Context = DbFactory.CreateDbContext();

            var count = rnd.Next(1, 4);

            Items = Context.BindItems.Take(count);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                Context?.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
