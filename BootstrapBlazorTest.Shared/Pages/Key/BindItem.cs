﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Aufo.Web
{
    /// <summary>
    /// 
    /// </summary>
    [Table("Test")]
    public class BindItem
    {
        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "主键")]
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "姓名不能为空")]
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DateTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "地址不能为空")]
        public string Address { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Complete { get; set; }
    }

    /// <summary>
    /// BindItemContext 上下文操作类
    /// </summary>
    public class BindItemDbContext : DbContext
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="options"></param>
        public BindItemDbContext(DbContextOptions<BindItemDbContext> options) : base(options)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<BindItem> BindItems { get; set; }
    }
}
